/*
This package is explicitly for Peruvian addresses.

Peruvian addresses are very different from US addreses.
*/
package direccion

import ()

type Direccion struct {
	Nombres      string
	Apellidos    string
	Calle1       string
	Calle2       string
	Calle3       string
	Localidad    string
	Distrito     string // Santiago de Surco
	Provincia    string // Lima
	Region       string // Lima
	Pais         string // Peru
	CodigoPostal string
}
